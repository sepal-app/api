FROM python:3.7-slim-buster AS builder

# Never prompts the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y \
  && apt-get install -y \
  build-essential \
  libpq-dev \
  libpython3.7-dev

RUN mkdir /deploy
WORKDIR /deploy
ENV VIRTUAL_ENV=/deploy/venv \
  PATH=$VIRTUAL_ENV/bin:$PATH
RUN python3 -m venv /deploy/venv
COPY requirements/prod.txt /deploy/requirements.txt
RUN . /deploy/venv/bin/activate \
  && pip3 install -U pip \
  && pip3 install -r requirements.txt

FROM python:3.7-slim-buster
RUN apt-get update -y \
  && apt-get install -y \
  libpq5
RUN mkdir /deploy
WORKDIR /deploy
ENV VIRTUAL_ENV=/deploy/venv \
  PATH=$VIRTUAL_ENV/bin:$PATH
COPY --from=builder /deploy/venv $VIRTUAL_ENV
COPY sepal/ /deploy/sepal/
COPY gunicorn.cfg /deploy/gunicorn.cfg
COPY manage.py /deploy/manage.py
COPY start.sh /deploy/start.sh
CMD ["/deploy/start.sh"]
