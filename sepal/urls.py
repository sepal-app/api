"""sepal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from rest_framework_nested import routers

from .v1.organizations.views import OrganizationViewSet
from .v1.taxa.views import TaxonViewSet
from .v1.users.views import UserViewSet, me
from .v1.accessions.views import AccessionItemViewSet, AccessionViewSet
from .v1.locations.views import LocationViewSet

router = routers.DefaultRouter()
router.register(r"orgs", OrganizationViewSet)
router.register(r"users", UserViewSet)

orgs_router = routers.NestedDefaultRouter(router, r"orgs", lookup="org")
orgs_router.register("taxa", TaxonViewSet, basename="taxon")

# TODO: we need a separate OrganizationUserViewSet that allows adding users to
# an organization and getting the list of users in an organization but nothing
# else on the user model
#
# orgs_router.register("users", UserViewSet, basename="user")
orgs_router.register("accessions", AccessionViewSet, basename="accession")
orgs_router.register("locations", LocationViewSet, basename="location")

accessions_router = routers.NestedDefaultRouter(
    orgs_router, r"accessions", lookup="accession"
)
accessions_router.register("items", AccessionItemViewSet, basename="accession-item")


v1_urls = router.urls + orgs_router.urls + accessions_router.urls + [path("me/", me)]

urlpatterns = [
    path("admin/", admin.site.urls),
    path("o/", include("oauth2_provider.urls", namespace="oauth2_provider")),
    path("v1/", include((v1_urls, "sepal_v1"))),
]
