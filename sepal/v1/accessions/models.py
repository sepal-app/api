from django.db import models
from typing_extensions import Final

from ..models.base import OrganizationMixin, TimestampedModelMixin


class Accession(TimestampedModelMixin, OrganizationMixin):
    code = models.CharField(max_length=64)
    taxon = models.ForeignKey("Taxon", on_delete="CASCADE", null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["organization_id", "code"], name="v1_accession_code_unique"
            )
        ]


ITEM_TYPES: Final = [
    ("plant", "Plant"),
    ("seed", "Seed/Spore"),
    ("vegetative", "Vegetative Part"),
    ("tissue", "Tissue Culture"),
    ("other", "Other"),
    ("", ""),
]


class AccessionItem(TimestampedModelMixin, OrganizationMixin):
    code = models.CharField(max_length=8)
    accession = models.ForeignKey(Accession, on_delete="CASCADE", null=False)
    location = models.ForeignKey("Location", on_delete="CASCADE", null=False)
    item_type = models.CharField(
        choices=ITEM_TYPES, max_length=32, blank=True, default=""
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["organization_id", "accession_id", "code"],
                name="v1_accession_item_code_unique",
            )
        ]
