from django.db.models import Q
from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from ..models import Location, Taxon
from .models import Accession, AccessionItem
from ..taxa.serializers import TaxonSerializer
from ..locations.serializers import LocationSerializer


class AccessionSerializer(FlexFieldsModelSerializer):
    taxon_id = serializers.PrimaryKeyRelatedField(
        allow_null=False, queryset=Taxon.objects.all(), source="taxon"
    )

    expandable_fields = {"taxon": (TaxonSerializer, {"read_only": True})}

    def save(self, **kwargs):
        # get the organization from the either the instance or the kwargs
        organization = getattr(self.instance, "organization", None) or kwargs.get(
            "organization", None
        )

        # if we couldn't get an organiation then try to look it up from the
        # organization_id
        if organization is None:
            organization_id = int(kwargs.get("organization_id", -1))
            if organization_id == -1:
                raise serializers.ValidationError(
                    "An organization is required for an AccessionItem"
                )
        else:
            organization_id = organization.id

        # get the taxon from either the validated data, the kwargs or from the
        # instance
        taxon = (
            self.validated_data.get("taxon", None)
            or kwargs.get("taxon", None)
            or getattr(self.instance, "taxon", None)
        )
        # TODO: I don't think this is necessary b/c the taxon should always be
        # available since its a required field
        #
        # if taxon is None:
        #     taxon_id = kwargs.get("taxon_id", None)
        #     if taxon_id is None:
        #         raise serializers.ValidationError(
        #             {"taxon_id": "An taxon is required for an TaxonItem"}
        #         )
        #     taxon = Taxon.objects.get(id=taxon_id)

        if taxon.organization_id != organization_id:
            raise serializers.ValidationError(
                {"taxon_id": "Taxon is in a different organization."}
            )

        code = self.validated_data.get("code", None) or getattr(
            self.instance, "code", None
        )

        # the code must be unique
        if Accession.objects.filter(
            Q(organization_id=organization_id)
            & Q(code=code)
            & ~Q(id=getattr(self.instance, "id", None))
        ).exists():
            raise serializers.ValidationError({"code": "The code must be unique."})

        return super().save(**kwargs)

    class Meta:
        model = Accession
        fields = ("code", "id", "taxon_id")


class AccessionItemSerializer(FlexFieldsModelSerializer):
    accession_id = serializers.PrimaryKeyRelatedField(
        allow_null=False, source="accession", read_only=True
    )
    location_id = serializers.PrimaryKeyRelatedField(
        allow_null=False, queryset=Location.objects.all(), source="location"
    )

    def save(self, **kwargs):
        organization = kwargs.get("organization", None) or getattr(
            self.instance, "organization", None
        )
        if organization is None:
            organization_id = int(kwargs.get("organization_id", -1))
            if organization_id == -1:
                raise serializers.ValidationError(
                    "An organization is required for an AccessionItem"
                )
        else:
            organization_id = organization.id

        accession = kwargs.get("accession", None) or getattr(
            self.instance, "accession", None
        )
        if accession is None:
            accession_id = kwargs.get("accession_id", None)
            if accession_id is None:
                raise serializers.ValidationError(
                    "An accession is required for an AccessionItem"
                )
            accession = Accession.objects.get(id=accession_id)

        location = self.validated_data.get("location", None) or getattr(
            self.instance, "location", None
        )

        if accession.organization_id != organization_id:
            raise serializers.ValidationError(
                "Accession is in a different organization."
            )

        if location.organization_id != organization_id:
            raise serializers.ValidationError(
                {"location_id": "Location is in a different organization."}
            )

        return super().save(**kwargs)

    expandable_fields = {
        "accession": (AccessionSerializer, {"read_only": True}),
        "location": (LocationSerializer, {"read_only": True}),
    }

    class Meta:
        model = AccessionItem
        fields = ("code", "id", "accession_id", "location_id", "item_type")
