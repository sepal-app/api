from rest_flex_fields import FlexFieldsModelViewSet, is_expanded

from .serializers import AccessionItemSerializer, AccessionSerializer
from .models import Accession, AccessionItem
from ..organizations.views import OrganizationModelViewSet


class AccessionViewSet(FlexFieldsModelViewSet, OrganizationModelViewSet):
    queryset = Accession.objects.none()  # Required for DjangoModelPermissions
    serializer_class = AccessionSerializer
    org_action_map = {"destroy": ("owner", "admin", "write")}
    search_fields = ("code",)
    permit_list_expands = ["taxon"]

    def get_queryset(self):
        org_id = self.get_organization_pk()
        queryset = Accession.objects.filter(organization_id=org_id)
        if is_expanded(self.request, "taxon"):
            queryset = queryset.select_related("taxon")
        return queryset


class AccessionItemViewSet(OrganizationModelViewSet):
    queryset = AccessionItem.objects.none()  # Required for DjangoModelPermissions
    serializer_class = AccessionItemSerializer
    org_action_map = {"destroy": ("owner", "admin", "write")}

    def get_queryset(self):
        org_id = self.kwargs["org_pk"]
        # TODO: can we just filter on the accession id and make sure the
        # accession is in the current org
        taxa = AccessionItem.objects.filter(organization_id=org_id)
        return taxa

    def perform_create(self, serializer):
        serializer.save(
            organization_id=self.get_organization_pk(),
            accession_id=self.kwargs["accession_pk"],
        )
