from django.contrib import admin

from .models import (
    Accession,
    AccessionItem,
    Location,
    Organization,
    OrganizationUser,
    Profile,
    Taxon,
    VernacularName,
)

admin.site.register(Accession)
admin.site.register(AccessionItem)
admin.site.register(Location)
admin.site.register(Organization)
admin.site.register(OrganizationUser)
admin.site.register(Profile)
admin.site.register(Taxon)
admin.site.register(VernacularName)
