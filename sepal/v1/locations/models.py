from django.db import models

from ..models.base import OrganizationMixin, TimestampedModelMixin


class Location(TimestampedModelMixin, OrganizationMixin):
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=20)
    description = models.TextField(blank=True, default="")
