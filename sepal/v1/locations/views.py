from rest_flex_fields import FlexFieldsModelViewSet

from .serializers import LocationSerializer
from .models import Location
from ..organizations.views import OrganizationModelViewSet


class LocationViewSet(FlexFieldsModelViewSet, OrganizationModelViewSet):
    queryset = Location.objects.none()  # Required for DjangoModelPermissions
    serializer_class = LocationSerializer
    org_action_map = {"destroy": ("owner", "admin", "write")}
    search_fields = ("name", "code")

    def get_queryset(self):
        org_id = self.get_organization_pk()
        return Location.objects.filter(organization_id=org_id)
