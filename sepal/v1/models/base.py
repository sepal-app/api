from django.db import models


class TimestampedModelMixin(models.Model):
    created: models.DateField = models.DateField(auto_now_add=True)
    modified: models.DateField = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class OrganizationMixin(models.Model):
    organization: models.ForeignKey = models.ForeignKey(
        "Organization", on_delete=models.CASCADE, null=False
    )

    class Meta:
        abstract = True
        unique_together = (("organization", "id"),)
