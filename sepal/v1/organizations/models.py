from django.contrib.auth.models import User
from django.db import models

from ..models.base import OrganizationMixin, TimestampedModelMixin

# TODO: need to define exactly what these different roles are for and their permissions
ROLES = (
    ("admin", "Admin"),
    ("read", "Read"),
    ("write", "Write"),
    ("owner", "Owner"),
)  #


class OrganizationManager(models.Manager):
    def get_for_user(self, user):
        return self.get_queryset().filter(users=user)


class Organization(TimestampedModelMixin):
    name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=128, blank=True)
    abbreviation = models.CharField(max_length=12, blank=True)
    is_active = models.BooleanField(default=False)
    objects = OrganizationManager()

    users = models.ManyToManyField(
        User, through="OrganizationUser", related_name="organizations"
    )


class OrganizationUser(TimestampedModelMixin, OrganizationMixin):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    role = models.CharField(max_length=32, choices=ROLES, default="read")
