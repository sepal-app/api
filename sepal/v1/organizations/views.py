from abc import ABC
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .serializers import OrganizationSerializer
from .models import Organization
from ..permissions import HasOrgRoleAction


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.none()  # Required for DjangoModelPermissions
    serializer_class = OrganizationSerializer

    org_action_map = {
        "create": ("__all__",),
        "update": ("admin", "owner"),
        "partial_update": ("admin", "owner"),
        "destroy": ("owner",),
    }

    def perform_create(self, serializer):
        org = serializer.save()
        org.users.add(self.request.user, through_defaults={"role": "owner"})

    def get_organization_pk(self):
        return self.kwargs.get("pk", None)

    def get_queryset(self):
        return self.request.user.organizations.all()

    def get_permissions(self):
        permission_classes = [IsAuthenticated, HasOrgRoleAction]
        return [permission() for permission in permission_classes]


class OrganizationModelViewSet(viewsets.ModelViewSet, ABC):
    """A model viewset for models that use the OrganizationMixin.

    This class adds features for working with models that are members of an
    organization.

    """

    # lookup_fields = ("organization", "pk")

    # def get_object(self):
    #     # TODO: filter objects by obj_pk
    #     pass

    def get_organization_pk(self):
        return self.kwargs["org_pk"]

    def get_permissions(self):
        permission_classes = [IsAuthenticated, HasOrgRoleAction]
        return [permission() for permission in permission_classes]

    def perform_create(self, serializer):
        serializer.save(organization_id=self.get_organization_pk())
