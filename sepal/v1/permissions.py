from rest_framework.permissions import BasePermission

from .models import OrganizationUser


class HasOrgRoleAction(BasePermission):

    default_action_map = {
        "create": ("owner", "admin", "write"),
        "destroy": ("owner", "admin", "write"),
        "list": ("__all__",),
        "partial_update": ("owner", "admin", "write"),
        "retrieve": ("__all__",),
        "update": ("owner", "admin", "write"),
    }

    def _get_action_roles(self, view):
        action_map = self.default_action_map.copy()
        if hasattr(view, "org_action_map"):
            action_map.update(view.org_action_map)

        roles = action_map.get(view.action, ("owner",))
        if "owner" not in roles:
            roles = roles + ("owner",)

        return roles

    def has_permission(self, request, view):
        if view.action not in ["list", "create"]:
            return True

        roles = self._get_action_roles(view)
        if roles is None or len(roles) == 0:
            return False

        org_id = view.get_organization_pk()
        if org_id is None and view.action in ["list", "create"]:
            # If the resource isn't part of an organization then allow anyone
            # to list or create
            return True

        query = OrganizationUser.objects.filter(user=request.user)
        if org_id is not None:
            query = query.filter(organization_id=org_id)
        if "__all__" not in roles:
            query = query.filter(role__in=roles)

        return query.exists()

    def has_object_permission(self, request, view, obj):
        roles = self._get_action_roles(view)
        if roles is None or len(roles) == 0:
            return False

        query = OrganizationUser.objects.filter(
            user=request.user, organization_id=view.get_organization_pk()
        )

        if "__all__" not in roles:
            query = query.filter(role__in=roles)

        return query.exists()
