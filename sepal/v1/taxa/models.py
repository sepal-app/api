from django.db import models

from ..models.base import OrganizationMixin, TimestampedModelMixin

RANKS = (
    ("family", "Family"),
    ("genus", "Genus"),
    ("species", "Species"),
    ("variety", "Variety"),
    ("subspecies", "Subspecies"),
    ("subvariety", "Subvariety"),
    ("form", "Form"),
    ("subform", "Subform"),
)

INFRASPECIFIC_ABBREVIATIONS = {
    ("variety", "var."),
    ("subspecies", "subsp."),
    ("subvariety", "subvar."),
    ("form", "f."),
    ("subform", "subf."),
}


class Taxon(TimestampedModelMixin, OrganizationMixin):
    name = models.CharField(max_length=128)
    rank = models.CharField(max_length=64, choices=RANKS)

    parent = models.ForeignKey(
        "self", on_delete="CASCADE", null=True, related_name="children"
    )

    synonyms = models.ManyToManyField("self")

    # TODO: provide a function for on_delete to automatically pick the first
    # vernacular name in the list
    default_vernacular_name = models.ForeignKey(
        "VernacularName", on_delete=models.PROTECT, related_name="+", null=True
    )

    # TODO: other things:
    # - distribution
    # - notes
    # - qualifier (senso stricto, lato)
    # - author
    # - hybrid flag

    @property
    def ancestors(self):
        a = list(self.get_ancestors().all())
        print(a)
        return a

    def get_ancestors(self, *args, **kwargs):
        # TODO: previously this was done with django-mptt but we should be able
        # to fudge it with SQL
        return super().get_ancestors(*args, ascending=True, **kwargs)

    class Meta:
        verbose_name_plural = "taxa"


# class Species(Taxon):
#     # hybrid = models.BooleanField(default=False)
#     # hybrid_taxon = models.ForeignKey(Taxon, null=True)
#     cv_group = models.CharField(max_length=64)
#     trade_name = models.CharField(max_length=64)

#     # TODO: if taxon search matches any rank below family, e.g. genus, species,
#     # we should return all the children of that taxon since it makes up part of
#     # the name

#     # sp_qual = Column(types.Enum(values=['agg.', 's. lat.', 's. str.', None]),
#     #                  default=None)


class VernacularName(TimestampedModelMixin, OrganizationMixin):
    name = models.CharField(max_length=128)
    language = models.CharField(max_length=64)

    taxon = models.ForeignKey(
        Taxon, on_delete=models.CASCADE, null=False, related_name="vernacular_names"
    )
