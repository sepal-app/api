from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from .models import Taxon, VernacularName


class VernacularNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = VernacularName
        fields = ("id", "name", "language")


class TaxonSerializer(FlexFieldsModelSerializer):
    parent_id = serializers.PrimaryKeyRelatedField(
        allow_null=True, queryset=Taxon.objects.all(), required=False, source="parent"
    )
    default_vernacular_name_id = serializers.PrimaryKeyRelatedField(
        queryset=VernacularName.objects.all(),
        allow_null=True,
        required=False,
        source="default_vernacular_name",
    )

    expandable_fields = {
        "default_vernacular_name": (VernacularNameSerializer, {"read_only": True}),
        "vernacular_names": (
            VernacularNameSerializer,
            {"many": True, "read_only": True},
        ),
        "parent": (
            "sepal.v1.taxa.serializers.TaxonSerializer",
            {"source": "parent", "read_only": True},
        ),
        "ancestors": (
            "sepal.v1.taxa.serializers.TaxonSerializer",
            {"many": True, "read_only": True},
        ),
    }

    class Meta:
        model = Taxon
        fields = ("id", "name", "rank", "parent_id", "default_vernacular_name_id")
