from rest_framework.decorators import action
from rest_framework.response import Response
from rest_flex_fields import FlexFieldsModelViewSet, is_expanded

from .serializers import TaxonSerializer  # , VernacularNameSerializer
from .models import Taxon, RANKS  # , VernacularName
from ..organizations.views import OrganizationModelViewSet


class TaxonViewSet(FlexFieldsModelViewSet, OrganizationModelViewSet):
    # DjangoModelPermissions requires a queryset attribute
    queryset = Taxon.objects.none()
    serializer_class = TaxonSerializer
    org_action_map = {"destroy": ("owner", "admin", "write")}
    search_fields = ("name",)
    permit_list_expands = ["parent"]

    def get_queryset(self):
        org_id = self.kwargs["org_pk"]
        queryset = Taxon.objects.filter(organization_id=org_id)
        if is_expanded(self.request, "default_vernacular_name"):
            queryset = queryset.select_related("default_vernacular_name")
        if is_expanded(self.request, "vernacular_names"):
            queryset = queryset.prefetch_related("vernacular_names")
        if is_expanded(self.request, "parent"):
            queryset = queryset.select_related("parent")
        if "rank" in self.request.GET:
            queryset = queryset.filter(rank=self.request.GET["rank"])
        return queryset

    @action(methods=["get"], detail=False)
    def meta(self, request, org_pk):
        ranks = [{"label": l, "value": v} for v, l in RANKS]
        return Response({"ranks": ranks})
