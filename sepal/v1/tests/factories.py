import factory
import factory.fuzzy as fuzzy
from django.contrib.auth.models import User

import sepal.v1.models as models


class UserFactory(factory.DjangoModelFactory):
    email = factory.Faker("email")
    username = factory.Faker("user_name")

    class Meta:
        model = User


class OrganizationFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Organization

    name = factory.Faker("first_name")


class OrganizationUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.OrganizationUser


class TaxonFactory(factory.DjangoModelFactory):
    name = fuzzy.FuzzyText()
    rank = fuzzy.FuzzyChoice(["family", "genus", "species"])

    class Meta:
        model = models.Taxon


class VernacularNameFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.VernacularName


class AccessionFactory(factory.DjangoModelFactory):
    code = fuzzy.FuzzyText()

    class Meta:
        model = models.Accession


class AccessionItemFactory(factory.DjangoModelFactory):
    code = fuzzy.FuzzyText(length=4)

    class Meta:
        model = models.AccessionItem


class LocationFactory(factory.DjangoModelFactory):
    code = fuzzy.FuzzyText(length=10)
    name = fuzzy.FuzzyText(length=32)
    description = fuzzy.FuzzyText(length=64)

    class Meta:
        model = models.Location
