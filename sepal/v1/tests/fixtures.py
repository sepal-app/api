import pytest
import secrets

from django.utils import timezone
from oauth2_provider.models import get_access_token_model, get_application_model
from rest_framework.test import APIRequestFactory, force_authenticate

# from oauth2_provider.settings import oauth2_settings

from .factories import (
    AccessionFactory,
    AccessionItemFactory,
    LocationFactory,
    TaxonFactory,
    OrganizationFactory,
    UserFactory,
)


Application = get_application_model()
AccessToken = get_access_token_model()


@pytest.fixture
def auth_request_factory(user, access_token):
    def inner(method, *args, **kwargs):
        request_factory = APIRequestFactory()
        req = getattr(request_factory, method.lower())(*args, **kwargs)
        # req = request_factory.get(*args, **kwargs)
        force_authenticate(req, user=user, token=access_token)
        return req

    return inner


@pytest.fixture
def make_token():
    def f(length=6):
        return secrets.token_hex()[0:length]

    return f


@pytest.fixture
def application(user):
    # oauth2_settings._SCOPES = ["read", "write", "admin"]

    app = Application.objects.create(
        name="Test Application",
        redirect_uris="http://localhost http://example.com http://example.org",
        user=user,
        client_type=Application.CLIENT_CONFIDENTIAL,
        authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
    )
    app.save()
    return app


@pytest.fixture
def access_token(user, application):
    access_token = AccessToken.objects.create(
        user=user,
        scope="read write admin",
        expires=timezone.now() + timezone.timedelta(seconds=300),
        token="secret-access-token-key",
        application=application,
    )
    # access_token.scope = "read write admin"
    access_token.save()
    return access_token


@pytest.fixture
def user():
    return UserFactory()


@pytest.fixture
def organization(user):
    org = OrganizationFactory()
    org.users.add(user, through_defaults={"role": "owner"})
    org.save()
    return org


@pytest.fixture
def make_org_with_role(user):
    def inner(role, user=user):
        org = OrganizationFactory()
        org.users.add(user, through_defaults={"role": role})
        org.save()
        return org

    return inner


@pytest.fixture
def taxon(organization):
    return TaxonFactory(organization=organization)


@pytest.fixture
def accession(organization, taxon):
    return AccessionFactory(organization=organization, taxon=taxon)


@pytest.fixture
def accession_item(organization, taxon, accession, location):
    return AccessionItemFactory(
        organization=organization, accession=accession, location=location
    )


@pytest.fixture
def location(organization):
    return LocationFactory(organization=organization)
