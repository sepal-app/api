import pytest
from django.urls import reverse

from sepal.v1.accessions.models import AccessionItem
from sepal.v1.accessions.views import AccessionItemViewSet
from sepal.v1.accessions.serializers import AccessionItemSerializer
from .factories import (
    AccessionItemFactory,
    AccessionFactory,
    LocationFactory,
    TaxonFactory,
    OrganizationFactory,
)
from .fixtures import *  # noqa: F401, F403
from ..organizations.models import ROLES

role_values = [role[0] for role in ROLES]

# TODO: need to make sure we can't add accession items to locations that aren't
# in the current organization

# TODO: we don't need an organization_id on accession items since we don't search on them


@pytest.fixture
def view():
    return AccessionItemViewSet.as_view()


@pytest.fixture
def accession_item_list_view():
    return AccessionItemViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def accession_item_detail_view():
    return AccessionItemViewSet.as_view(
        {
            "get": "retrieve",
            "delete": "destroy",
            "put": "update",
            "patch": "partial_update",
        }
    )


@pytest.fixture
def non_org_accession_item(make_item_for_org):
    """Return a accession for which the user is not a member."""
    org = OrganizationFactory()
    return make_item_for_org(org)


@pytest.fixture
def make_item_for_org():
    def _inner(org):
        return AccessionItemFactory(
            organization=org,
            accession=AccessionFactory(
                organization=org, taxon=TaxonFactory(organization=org)
            ),
            location=LocationFactory(organization=org),
        )

    return _inner


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_accession_list(
    auth_request_factory,
    accession_item_list_view,
    make_org_with_role,
    user,
    role,
    make_item_for_org,
):
    """List the accessions on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    item = make_item_for_org(org)
    url = reverse(
        "sepal_v1:accession-item-list",
        kwargs={"org_pk": org.pk, "accession_pk": item.accession.pk},
    )
    req = auth_request_factory("get", url)
    res = accession_item_list_view(req, org_pk=org.pk, accession_ok=item.accession.pk)
    assert res.status_code == 200
    assert res.data["results"] == [AccessionItemSerializer(item).data]


@pytest.mark.django_db
def test_accession_item_list_non_org_fails(
    auth_request_factory, accession_item_list_view, non_org_accession_item
):
    """List the accessions for an organization that the user is not a member of fails."""
    url = reverse(
        "sepal_v1:accession-item-list",
        kwargs={
            "org_pk": non_org_accession_item.organization.pk,
            "accession_pk": non_org_accession_item.accession.pk,
        },
    )
    req = auth_request_factory("get", url)
    res = accession_item_list_view(req, org_pk=non_org_accession_item.organization.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_accession_item_detail(
    auth_request_factory,
    accession_item_detail_view,
    make_org_with_role,
    role,
    make_item_for_org,
):
    """Get the details of an accession item succeeds for all roles."""
    org = make_org_with_role(role)
    item = make_item_for_org(org)
    url = reverse(
        "sepal_v1:accession-item-detail", args=[org.pk, item.accession.pk, item.pk]
    )
    req = auth_request_factory("get", url)
    res = accession_item_detail_view(
        req, org_pk=org.pk, accession_ok=item.accession.pk, pk=item.pk
    )
    assert res.status_code == 200
    assert res.data == AccessionItemSerializer(item).data


@pytest.mark.django_db
def test_accession_item_detail_non_org_fails(
    auth_request_factory,
    accession_item_detail_view,
    organization,
    non_org_accession_item,
):
    """Get the the accession details for an organization that the user is not a member
of fails."""
    url = reverse(
        "sepal_v1:accession-item-detail",
        args=[
            organization.pk,
            non_org_accession_item.accession.pk,
            non_org_accession_item.pk,
        ],
    )
    req = auth_request_factory("get", url)
    res = accession_item_detail_view(
        req,
        org_pk=organization.pk,
        accession_pk=non_org_accession_item.accession.pk,
        pk=non_org_accession_item.pk,
    )
    assert res.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_item_create(
    auth_request_factory,
    accession_item_list_view,
    make_token,
    make_org_with_role,
    role,
    accession,
):
    """Create accession for an organization with owner, admin and write succeeds.."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    accession = AccessionFactory(organization=org, taxon=TaxonFactory(organization=org))
    url = reverse("sepal_v1:accession-item-list", args=[org.pk, accession.pk])
    data = {"code": make_token(), "location_id": location.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_item_list_view(req, org_pk=org.pk, accession_pk=accession.pk)
    assert res.status_code == 201, res.content
    assert res.data == dict(
        **data, id=res.data["id"], accession_id=accession.id, item_type=""
    )
    item = AccessionItem.objects.get(id=res.data["id"])
    assert item.organization == org


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_accession_item_create_permission_fails(
    auth_request_factory,
    accession_item_list_view,
    make_token,
    make_org_with_role,
    role,
    accession,
    location,
):
    org = make_org_with_role(role)
    url = reverse("sepal_v1:accession-item-list", args=[org.pk, accession.pk])
    data = {"code": make_token(), "location_id": location.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_item_list_view(req, org_pk=org.pk, accession_pk=accession.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_accession_item_create_non_org_fails(
    auth_request_factory, accession_item_list_view, make_token, accession, location
):
    org = OrganizationFactory()
    url = reverse("sepal_v1:accession-item-list", args=[org.pk, accession.pk])
    data = {"code": make_token(), "location_id": location.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_item_list_view(req, org_pk=org.pk, accession_pk=accession.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_accession_item_create_non_org_location_fails(
    auth_request_factory, accession_item_list_view, make_token, accession, location
):
    org = OrganizationFactory()
    non_org_location = LocationFactory(organization=org)
    url = reverse(
        "sepal_v1:accession-item-list", args=[accession.organization.pk, accession.pk]
    )
    data = {"code": make_token(), "location_id": non_org_location.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_item_list_view(
        req, org_pk=accession.organization.pk, accession_pk=accession.pk
    )
    assert res.status_code == 400, res.content
    assert (
        res.content
        == b'{"detail": {"location_id": "Location is in a different organization."}}'
    )


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_item_update(
    auth_request_factory,
    accession_item_detail_view,
    make_token,
    make_org_with_role,
    role,
    make_item_for_org,
):
    """Update accession for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    item = make_item_for_org(org)
    url = reverse(
        "sepal_v1:accession-item-detail", args=[org.pk, item.accession.pk, item.pk]
    )
    data = {"code": make_token(), "location_id": item.location.id}
    req = auth_request_factory("put", url, data, format="json")
    res = accession_item_detail_view(
        req, org_pk=org.pk, accession_pk=item.accession.pk, pk=item.pk
    )
    assert res.status_code == 200, res.content
    assert res.data == dict(
        **data, id=res.data["id"], accession_id=item.accession.id, item_type=""
    )


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_item_partial_update(
    auth_request_factory,
    accession_item_detail_view,
    make_token,
    make_org_with_role,
    role,
    make_item_for_org,
):
    """Update accession for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    item = make_item_for_org(org)
    new_location = LocationFactory(organization=org)
    url = reverse(
        "sepal_v1:accession-item-detail", args=[org.pk, item.accession.pk, item.pk]
    )
    data = {"code": make_token(), "location_id": new_location.id}
    req = auth_request_factory("patch", url, data, format="json")
    res = accession_item_detail_view(
        req, org_pk=org.pk, accession_pk=item.accession.pk, pk=item.pk
    )
    assert res.status_code == 200, res.content
    assert res.data["id"] == item.id
    assert res.data["location_id"] == new_location.id


@pytest.mark.django_db
def test_accession_item_delete(
    auth_request_factory, accession_item_detail_view, organization, accession_item
):
    url = reverse(
        "sepal_v1:accession-item-detail",
        args=[organization.pk, accession_item.accession.pk, accession_item.pk],
    )
    req = auth_request_factory("delete", url)
    res = accession_item_detail_view(
        req,
        org_pk=organization.pk,
        accession_pk=accession_item.accession.pk,
        pk=accession_item.pk,
    )
    assert res.status_code == 204


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_accession_item_delete_permissions_fails(
    auth_request_factory,
    accession_item_detail_view,
    make_org_with_role,
    role,
    make_item_for_org,
):
    """Test the a user can't delete an accession item they don't own."""
    org = make_org_with_role(role)
    item = make_item_for_org(org)
    url = reverse(
        "sepal_v1:accession-item-detail", args=[org.pk, item.accession.pk, item.pk]
    )
    req = auth_request_factory("delete", url)
    res = accession_item_detail_view(
        req, org_pk=org.pk, accession_pk=item.accession.pk, pk=item.pk
    )
    assert res.status_code == 403
