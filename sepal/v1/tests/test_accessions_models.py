import pytest

from .fixtures import *  # noqa: F401, F403
from .factories import AccessionFactory, AccessionItemFactory


@pytest.mark.django_db
def test_accession_model(organization, taxon):
    accession = AccessionFactory(organization=organization, taxon=taxon)
    assert isinstance(accession.id, int)


@pytest.mark.django_db
def test_accession_item_model(organization, accession, location):
    item = AccessionItemFactory(
        organization=organization, accession=accession, location=location
    )
    assert isinstance(item.id, int)
