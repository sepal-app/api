import pytest
from rest_framework import serializers

from .fixtures import *  # noqa: F401, F403
from .factories import OrganizationFactory, TaxonFactory
from sepal.v1.accessions.serializers import AccessionSerializer, AccessionItemSerializer


@pytest.mark.django_db
def test_accession_serialize(organization, accession):
    data = AccessionSerializer(accession).data
    assert data["code"] == accession.code
    assert data["id"] == accession.id
    assert data["taxon_id"] == accession.taxon.id


@pytest.mark.django_db
def test_accession_create(organization, make_token, taxon):
    data = {"code": make_token(), "taxon_id": taxon.id}
    serializer = AccessionSerializer(data=data)
    assert serializer.is_valid(), serializer.errors
    accession = serializer.save(organization=organization)
    assert accession.id is not None
    assert accession.organization_id == organization.id
    assert data["code"] == accession.code
    assert data["taxon_id"] == taxon.id


@pytest.mark.django_db
def test_accession_create_taxon_wrong_org(organization, make_token):
    other_org = OrganizationFactory()
    taxon = TaxonFactory(organization=other_org)
    data = {"code": make_token(), "taxon_id": taxon.id}
    serializer = AccessionSerializer(data=data)
    assert serializer.is_valid(), serializer.errors
    with pytest.raises(serializers.ValidationError) as exc:
        serializer.save(organization=organization)

    assert "taxon_id" in exc.value.detail
    assert "organization" in exc.value.detail["taxon_id"]


@pytest.mark.django_db
def test_accession_update(organization, taxon, accession, make_token):
    data = {"code": make_token(), "taxon_id": taxon.id}
    serializer = AccessionSerializer(accession, data=data)
    assert serializer.is_valid(), serializer.errors
    serializer.save()
    assert accession.code == data["code"]
    assert accession.taxon.id == data["taxon_id"]


@pytest.mark.django_db
def test_accession_partial_update(organization, taxon, accession, make_token):
    data = {"code": make_token()}
    serializer = AccessionSerializer(accession, data=data, partial=True)
    assert serializer.is_valid(), serializer.errors
    serializer.save()
    assert accession.code == data["code"]
    assert accession.taxon_id == taxon.id


@pytest.mark.django_db
def test_accession_item_serialize(organization, accession_item):
    data = AccessionItemSerializer(accession_item).data
    assert data["code"] == accession_item.code
    assert data["id"] == accession_item.id
    assert data["accession_id"] == accession_item.accession.id


@pytest.mark.django_db
def test_accession_item_create(organization, make_token, accession, location):
    data = {"code": make_token(), "location_id": location.id}
    serializer = AccessionItemSerializer(data=data)
    assert serializer.is_valid(), serializer.errors
    accession_item = serializer.save(organization=organization, accession=accession)
    assert accession_item.id is not None
    assert accession_item.organization_id == organization.id
    assert accession_item.code == data["code"]
    assert accession_item.location_id == data["location_id"]
    assert accession_item.accession_id == accession.id


@pytest.mark.django_db
def test_accession_item_update(
    organization, accession, accession_item, location, make_token
):
    data = {
        "code": make_token(),
        "accession_id": accession.id,
        "location_id": location.id,
    }
    serializer = AccessionItemSerializer(accession_item, data=data)
    assert serializer.is_valid(), serializer.errors
    serializer.save(organization=organization)
    assert accession_item.code == data["code"]
    assert accession_item.accession.id == data["accession_id"]


@pytest.mark.django_db
def test_accession_item_partial_update(
    organization, accession, accession_item, make_token
):
    data = {"code": make_token(6)}
    serializer = AccessionItemSerializer(accession_item, data=data, partial=True)
    assert serializer.is_valid(), serializer.errors
    serializer.save(organization=organization, accession=accession)
    assert accession_item.code == data["code"]
    assert accession_item.accession == accession
