import pytest
from django.urls import reverse

from sepal.v1.accessions.views import AccessionViewSet

from sepal.v1.accessions.serializers import AccessionSerializer
from .factories import AccessionFactory, TaxonFactory, OrganizationFactory
from .fixtures import *  # noqa: F401, F403
from ..organizations.models import ROLES

role_values = [role[0] for role in ROLES]


@pytest.fixture
def view():
    return AccessionViewSet.as_view()


@pytest.fixture
def accession_list_view():
    return AccessionViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def accession_detail_view():
    return AccessionViewSet.as_view(
        {
            "get": "retrieve",
            "delete": "destroy",
            "put": "update",
            "patch": "partial_update",
        }
    )


@pytest.fixture
def non_org_accession():
    """Return a accession for which the user is not a member."""
    taxon = TaxonFactory(organization=OrganizationFactory())
    return AccessionFactory(organization=OrganizationFactory(), taxon=taxon)


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_accession_list(
    auth_request_factory, accession_list_view, make_org_with_role, user, role, taxon
):
    """List the accessions on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    accession = AccessionFactory(organization=org, taxon=taxon)
    url = reverse("sepal_v1:accession-list", kwargs={"org_pk": org.pk})
    req = auth_request_factory("get", url)
    res = accession_list_view(req, org_pk=org.pk)
    assert res.status_code == 200
    assert res.data["results"] == [AccessionSerializer(accession).data]


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_accession_list_search(
    auth_request_factory, accession_list_view, make_org_with_role, user, role, taxon
):
    """List the accessions on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    accession = AccessionFactory(organization=org, taxon=taxon)
    AccessionFactory(organization=org, taxon=taxon)  # another accession in same org
    AccessionFactory(
        code=accession.code, organization=OrganizationFactory(), taxon=taxon
    )  # another accession in different org with same code

    url = reverse("sepal_v1:accession-list", kwargs={"org_pk": org.pk})
    url += f"?search={accession.code}"
    req = auth_request_factory("get", url)
    res = accession_list_view(req, org_pk=org.pk)
    print(res.data["results"])
    assert res.status_code == 200
    assert res.data["count"] == 1
    assert len(res.data["results"]) == 1
    assert res.data["results"][0]["id"] == accession.id


@pytest.mark.django_db
def test_accession_list_non_org_fails(
    auth_request_factory, accession_list_view, non_org_accession
):
    """List the accessions for an organization that the user is not a member of fails."""
    url = reverse(
        "sepal_v1:accession-list", kwargs={"org_pk": non_org_accession.organization.pk}
    )
    req = auth_request_factory("get", url)
    res = accession_list_view(req, org_pk=non_org_accession.organization.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_accession_detail(
    auth_request_factory, accession_detail_view, make_org_with_role, role, taxon
):
    """Get the details of a accession succeeds for all roles."""
    org = make_org_with_role(role)
    accession = AccessionFactory(organization=org, taxon=taxon)
    url = reverse("sepal_v1:accession-detail", args=[org.pk, accession.pk])
    req = auth_request_factory("get", url)
    res = accession_detail_view(req, org_pk=org.pk, pk=accession.pk)
    assert res.status_code == 200
    assert res.data == AccessionSerializer(accession).data


@pytest.mark.django_db
def test_accession_detail_non_org_fails(
    auth_request_factory, accession_detail_view, organization, non_org_accession
):
    """Get the the accession details for an organization that the user is not a member
of fails."""
    url = reverse(
        "sepal_v1:accession-detail", args=[organization.pk, non_org_accession.pk]
    )
    req = auth_request_factory("get", url)
    res = accession_detail_view(req, org_pk=organization.pk, pk=non_org_accession.pk)
    assert res.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_create(
    auth_request_factory, accession_list_view, make_token, make_org_with_role, role
):
    """Create accession for an organization with owner, admin and write succeeds.."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    url = reverse("sepal_v1:accession-list", args=[org.pk])
    data = {"code": make_token(), "taxon_id": taxon.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_list_view(req, org_pk=org.pk)
    assert res.status_code == 201
    assert res.data == dict(**data, id=res.data["id"])


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_accession_create_permission_fails(
    auth_request_factory,
    accession_list_view,
    make_token,
    make_org_with_role,
    role,
    taxon,
):
    org = make_org_with_role(role)
    url = reverse("sepal_v1:accession-list", args=[org.pk])
    data = {"code": make_token(), "taxon_id": taxon.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_accession_create_non_org_fails(
    auth_request_factory, accession_list_view, make_token, taxon
):
    org = OrganizationFactory()
    url = reverse("sepal_v1:accession-list", args=[org.pk])
    data = {"code": make_token(), "taxon_id": taxon.id}
    req = auth_request_factory("post", url, data, format="json")
    res = accession_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_update(
    auth_request_factory, accession_detail_view, make_token, make_org_with_role, role
):
    """Update accession for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    accession = AccessionFactory(organization=org, taxon=TaxonFactory(organization=org))
    url = reverse("sepal_v1:accession-detail", args=[org.pk, accession.pk])
    data = {"code": make_token(), "taxon_id": accession.taxon.id}
    req = auth_request_factory("put", url, data, format="json")
    res = accession_detail_view(req, org_pk=org.pk, pk=accession.pk)
    assert res.status_code == 200
    assert res.data == dict(**data, id=res.data["id"])


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_accession_partial_update(
    auth_request_factory, accession_detail_view, make_token, make_org_with_role, role
):
    """Update accession for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    accession = AccessionFactory(organization=org, taxon=taxon)
    new_taxon = TaxonFactory(organization=org)
    url = reverse("sepal_v1:accession-detail", args=[org.pk, accession.pk])
    data = {"code": make_token(), "taxon_id": new_taxon.id}
    req = auth_request_factory("patch", url, data, format="json")
    res = accession_detail_view(req, org_pk=org.pk, pk=accession.pk)
    assert res.status_code == 200
    assert res.data == dict(**data, id=res.data["id"])


@pytest.mark.django_db
def test_accession_delete(
    auth_request_factory, accession_detail_view, organization, accession
):
    url = reverse("sepal_v1:accession-detail", args=[organization.pk, accession.pk])
    req = auth_request_factory("delete", url)
    res = accession_detail_view(req, org_pk=organization.pk, pk=accession.pk)
    assert res.status_code == 204


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_accession_delete_permissions_fails(
    auth_request_factory, accession_detail_view, make_org_with_role, role
):
    """Test the a user can't delete a accession they don't own."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    accession = AccessionFactory(organization=org, taxon=taxon)
    url = reverse("sepal_v1:accession-detail", args=[org.pk, accession.pk])
    req = auth_request_factory("delete", url)
    res = accession_detail_view(req, org_pk=org.pk, pk=accession.pk)
    assert res.status_code == 403
