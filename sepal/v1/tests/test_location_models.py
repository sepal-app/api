import pytest

from .fixtures import *  # noqa: F401, F403
from .factories import LocationFactory


@pytest.mark.django_db
def test_location_model(organization):
    location = LocationFactory(organization=organization)
    assert isinstance(location.id, int)
