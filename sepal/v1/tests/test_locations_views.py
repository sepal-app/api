import pytest
from django.urls import reverse

from sepal.v1.locations.views import LocationViewSet

from sepal.v1.locations.serializers import LocationSerializer
from .factories import LocationFactory, OrganizationFactory
from .fixtures import *  # noqa: F401, F403
from ..organizations.models import ROLES

role_values = [role[0] for role in ROLES]


@pytest.fixture
def view():
    return LocationViewSet.as_view()


@pytest.fixture
def location_list_view():
    return LocationViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def location_detail_view():
    return LocationViewSet.as_view(
        {
            "get": "retrieve",
            "delete": "destroy",
            "put": "update",
            "patch": "partial_update",
        }
    )


@pytest.fixture
def non_org_location():
    """Return a location for which the user is not a member."""
    return LocationFactory(organization=OrganizationFactory())


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_location_list(
    auth_request_factory, location_list_view, make_org_with_role, user, role
):
    """List the locations on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    url = reverse("sepal_v1:location-list", kwargs={"org_pk": org.pk})
    req = auth_request_factory("get", url)
    res = location_list_view(req, org_pk=org.pk)
    assert res.status_code == 200
    assert res.data["results"] == [LocationSerializer(location).data]


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_location_list_search(
    auth_request_factory, location_list_view, make_org_with_role, user, role
):
    """List the accessions on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)  # another location in same org
    LocationFactory(organization=org)  # another location in same org
    LocationFactory(
        code=location.code, organization=OrganizationFactory()
    )  # another location in different org with same codw

    url = reverse("sepal_v1:location-list", kwargs={"org_pk": org.pk})
    url += f"?search={location.code}"
    req = auth_request_factory("get", url)
    res = location_list_view(req, org_pk=org.pk)
    print(res.data["results"])
    assert res.status_code == 200
    assert res.data["count"] == 1
    assert len(res.data["results"]) == 1
    assert res.data["results"][0]["id"] == location.id


@pytest.mark.django_db
def test_location_list_non_org_fails(
    auth_request_factory, location_list_view, non_org_location
):
    """List the locations for an organization that the user is not a member of fails."""
    url = reverse(
        "sepal_v1:location-list", kwargs={"org_pk": non_org_location.organization.pk}
    )
    req = auth_request_factory("get", url)
    res = location_list_view(req, org_pk=non_org_location.organization.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_location_detail(
    auth_request_factory, location_detail_view, make_org_with_role, role
):
    """Get the details of a location succeeds for all roles."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    url = reverse("sepal_v1:location-detail", args=[org.pk, location.pk])
    req = auth_request_factory("get", url)
    res = location_detail_view(req, org_pk=org.pk, pk=location.pk)
    assert res.status_code == 200
    assert res.data == LocationSerializer(location).data


@pytest.mark.django_db
def test_location_detail_non_org_fails(
    auth_request_factory, location_detail_view, organization, non_org_location
):
    """Test get location fails if not a member of organization."""
    url = reverse(
        "sepal_v1:location-detail", args=[organization.pk, non_org_location.pk]
    )
    req = auth_request_factory("get", url)
    res = location_detail_view(req, org_pk=organization.pk, pk=non_org_location.pk)
    assert res.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_location_create(
    auth_request_factory, location_list_view, make_token, make_org_with_role, role
):
    """Create location for an organization with owner, admin and write succeeds.."""
    org = make_org_with_role(role)
    url = reverse("sepal_v1:location-list", args=[org.pk])
    data = {"code": make_token(), "name": make_token()}
    req = auth_request_factory("post", url, data, format="json")
    res = location_list_view(req, org_pk=org.pk)
    assert res.status_code == 201, res.content
    assert res.data == dict(**data, id=res.data["id"], description="")


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_location_create_permission_fails(
    auth_request_factory, location_list_view, make_token, make_org_with_role, role
):
    org = make_org_with_role(role)
    url = reverse("sepal_v1:location-list", args=[org.pk])
    data = {"code": make_token(), "name": make_token()}
    req = auth_request_factory("post", url, data, format="json")
    res = location_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_location_create_non_org_fails(
    auth_request_factory, location_list_view, make_token, taxon
):
    org = OrganizationFactory()
    url = reverse("sepal_v1:location-list", args=[org.pk])
    data = {"code": make_token(), "name": make_token()}
    req = auth_request_factory("post", url, data, format="json")
    res = location_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_location_update(
    auth_request_factory, location_detail_view, make_token, make_org_with_role, role
):
    """Update location for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    url = reverse("sepal_v1:location-detail", args=[org.pk, location.pk])
    data = {"code": make_token(), "name": make_token()}
    req = auth_request_factory("put", url, data, format="json")
    res = location_detail_view(req, org_pk=org.pk, pk=location.pk)
    assert res.status_code == 200
    assert res.data == dict(**data, id=res.data["id"], description=location.description)


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_location_partial_update(
    auth_request_factory, location_detail_view, make_token, make_org_with_role, role
):
    """Update location for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    url = reverse("sepal_v1:location-detail", args=[org.pk, location.pk])
    data = {"code": make_token()}
    req = auth_request_factory("patch", url, data, format="json")
    res = location_detail_view(req, org_pk=org.pk, pk=location.pk)
    assert res.status_code == 200
    assert res.data == dict(
        **data, id=res.data["id"], name=location.name, description=location.description
    )


@pytest.mark.django_db
def test_location_delete(
    auth_request_factory, location_detail_view, organization, location
):
    url = reverse("sepal_v1:location-detail", args=[organization.pk, location.pk])
    req = auth_request_factory("delete", url)
    res = location_detail_view(req, org_pk=organization.pk, pk=location.pk)
    assert res.status_code == 204


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_location_delete_permissions_fails(
    auth_request_factory, location_detail_view, make_org_with_role, role
):
    """Test the a user can't delete a location they don't own."""
    org = make_org_with_role(role)
    location = LocationFactory(organization=org)
    url = reverse("sepal_v1:location-detail", args=[org.pk, location.pk])
    req = auth_request_factory("delete", url)
    res = location_detail_view(req, org_pk=org.pk, pk=location.pk)
    assert res.status_code == 403
