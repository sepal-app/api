import pytest

from .factories import OrganizationFactory, OrganizationUserFactory, UserFactory


@pytest.mark.django_db
def test_organization_model():
    org = OrganizationFactory()
    assert isinstance(org.id, int)


@pytest.mark.django_db
def test_organization_user_model():
    org = OrganizationFactory()
    user = UserFactory()
    _ = OrganizationUserFactory(organization=org, user=user)

    assert isinstance(org.id, int)
    assert user in org.users.all()


@pytest.mark.django_db
def test_organization_users_relationship():
    org = OrganizationFactory()
    user = UserFactory()
    org.users.add(user)
    org.save()

    assert user in org.users.all()
