import pytest
from django.urls import reverse

from sepal.v1.organizations.models import OrganizationUser
from sepal.v1.organizations.views import OrganizationViewSet
from sepal.v1.organizations.serializers import OrganizationSerializer
from .factories import OrganizationFactory
from .fixtures import *  # noqa: F401, F403


@pytest.fixture
def view():
    return OrganizationViewSet.as_view()


@pytest.fixture
def organization_list_view():
    return OrganizationViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def organization_detail_view():
    return OrganizationViewSet.as_view({"get": "retrieve", "delete": "destroy"})


@pytest.fixture
def other_organization():
    return OrganizationFactory()


@pytest.mark.django_db
def test_organization_list(
    auth_request_factory, organization_list_view, organization, other_organization
):
    url = reverse("sepal_v1:organization-list")
    req = auth_request_factory("get", url)
    res = organization_list_view(req)
    assert res.status_code == 200
    assert res.data["results"] == [OrganizationSerializer(organization).data]


@pytest.mark.django_db
def test_organization_detail(
    auth_request_factory, organization_detail_view, organization, other_organization
):
    url = reverse("sepal_v1:organization-detail", args=[organization.pk])
    req = auth_request_factory("get", url)
    res = organization_detail_view(req, pk=organization.pk)
    assert res.status_code == 200
    assert res.data == OrganizationSerializer(organization).data


@pytest.mark.django_db
def test_organization_create(
    auth_request_factory, organization_list_view, make_token, user
):
    url = reverse("sepal_v1:organization-list")
    data = {"name": make_token()}
    req = auth_request_factory("post", url, data, format="json")
    res = organization_list_view(req)
    assert res.status_code == 201
    assert isinstance(res.data["id"], int)
    assert res.data["name"] == data["name"]
    assert (
        OrganizationUser.objects.filter(
            user_id=user.id, organization_id=res.data["id"], role="owner"
        ).count()
        == 1
    )


def test_organization_update():
    # TODO
    pass


def test_organization_partial_update():
    # TODO
    pass


@pytest.mark.django_db
def test_organization_delete(
    auth_request_factory, organization_detail_view, organization, other_organization
):
    url = reverse("sepal_v1:organization-detail", args=[organization.pk])
    req = auth_request_factory("delete", url)
    res = organization_detail_view(req, pk=organization.pk)
    assert res.status_code == 204


@pytest.mark.django_db
def test_organization_delete_other(
    auth_request_factory, organization_detail_view, organization, other_organization
):
    """Test the a user can't delete an organization they don't own."""
    url = reverse("sepal_v1:organization-detail", args=[other_organization.pk])
    req = auth_request_factory("delete", url)
    res = organization_detail_view(req, pk=other_organization.pk)
    assert res.status_code == 404
