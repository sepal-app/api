import pytest

from .fixtures import *  # noqa: F401, F403
from .factories import TaxonFactory, VernacularNameFactory


@pytest.mark.django_db
def test_taxon_model(taxon):
    assert isinstance(taxon.id, int)


@pytest.mark.django_db
def test_taxon_vernacular_name(organization, taxon, make_token):
    vernacular_name = VernacularNameFactory(organization=organization, taxon=taxon)
    assert vernacular_name in taxon.vernacular_names.all()


@pytest.mark.django_db
def test_taxon_default_vernacular_name(organization, taxon):
    vernacular_name = VernacularNameFactory(organization=organization, taxon=taxon)
    taxon.default_vernacular_name = vernacular_name
    assert vernacular_name in taxon.vernacular_names.all()
    taxon.default_vernacular_name_id = vernacular_name.pk


# @pytest.mark.django_db
# def test_taxon_parent_tree(organization):
#     taxon1 = TaxonFactory(organization=organization)
#     taxon2 = TaxonFactory(organization=organization, parent=taxon1)
#     taxon3 = TaxonFactory(organization=organization, parent=taxon2)

#     taxon3.get_ancestors() == [taxon2, taxon1]
