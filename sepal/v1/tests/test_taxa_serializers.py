import pytest

from .fixtures import *  # noqa: F401, F403
from .factories import TaxonFactory, VernacularNameFactory
from sepal.v1.taxa.serializers import TaxonSerializer, VernacularNameSerializer


@pytest.mark.django_db
def test_vernacular_name_serialize(organization, taxon):
    vernacular_name = VernacularNameFactory(taxon=taxon, organization=organization)
    data = VernacularNameSerializer(vernacular_name).data
    assert data["id"] == vernacular_name.id


@pytest.mark.django_db
def test_taxon_serialize(organization, taxon):
    data = TaxonSerializer(taxon).data
    assert data["name"] == taxon.name
    assert data["id"] == taxon.id
    assert data["parent_id"] is None


@pytest.mark.django_db
def test_taxon_create(organization, make_token):
    data = {"name": make_token(), "rank": "genus", "parent_id": None}
    serializer = TaxonSerializer(data=data)
    assert serializer.is_valid(), serializer.errors
    taxon = serializer.save(organization=organization)
    assert taxon.id is not None
    assert taxon.organization_id == organization.id
    assert data["name"] == taxon.name
    assert data["rank"] == taxon.rank


@pytest.mark.django_db
def test_taxon_create_nested(organization, make_token):
    # TODO:
    pass


@pytest.mark.django_db
def test_taxon_update(organization, taxon, make_token):
    parent = TaxonFactory(organization=organization)
    data = {"name": make_token(), "rank": "genus", "parent_id": parent.id}
    serializer = TaxonSerializer(taxon, data=data)
    assert serializer.is_valid(), serializer.errors
    serializer.save()
    assert taxon.name == data["name"]
    assert taxon.rank == data["rank"]
    assert taxon.parent == parent


@pytest.mark.django_db
def test_taxon_update_vernacular_names_fails(organization, make_token):
    # TODO:
    pass


@pytest.mark.django_db
def test_taxon_partial_update(organization, taxon, make_token):
    parent = TaxonFactory(organization=organization)
    data = {"parent_id": parent.id}
    serializer = TaxonSerializer(taxon, data=data, partial=True)
    assert serializer.is_valid(), serializer.errors
    serializer.save()
    assert taxon.parent == parent


# @pytest.mark.django_db
# def test_taxon_expand_ancestors(organization):
#     taxon1 = TaxonFactory(organization=organization)
#     taxon2 = TaxonFactory(organization=organization, parent=taxon1)
#     taxon3 = TaxonFactory(organization=organization, parent=taxon2)

#     data = TaxonSerializer(taxon3, expand=["ancestors"]).data
#     assert isinstance(data["ancestors"], list)
#     assert len(data["ancestors"]) == 2
#     assert data["ancestors"][0]["id"] == taxon2.id
#     assert data["ancestors"][1]["id"] == taxon1.id


@pytest.mark.django_db
def test_taxon_expand_parent(organization):
    taxon1 = TaxonFactory(organization=organization)
    taxon2 = TaxonFactory(organization=organization, parent=taxon1)

    data = TaxonSerializer(taxon2, expand=["parent"]).data
    assert data["parent_id"] == taxon1.id
    assert data["parent"]["id"] == taxon1.id
