import pytest
from django.urls import reverse

from sepal.v1.taxa.views import TaxonViewSet

from sepal.v1.taxa.serializers import TaxonSerializer
from .factories import TaxonFactory, OrganizationFactory
from .fixtures import *  # noqa: F401, F403
from ..organizations.models import ROLES

role_values = [role[0] for role in ROLES]


@pytest.fixture
def view():
    return TaxonViewSet.as_view()


@pytest.fixture
def taxon_list_view():
    return TaxonViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def taxon_detail_view():
    return TaxonViewSet.as_view(
        {
            "get": "retrieve",
            "delete": "destroy",
            "put": "update",
            "patch": "partial_update",
        }
    )


@pytest.fixture
def non_org_taxon():
    """Return a taxon for which the user is not a member."""
    return TaxonFactory(organization=OrganizationFactory())


@pytest.fixture
def taxon_nested_data(make_token):
    return {
        "name": make_token(),
        "rank": "species",
        "parent": {
            "name": make_token(),
            "rank": "genus",
            "parent": {"name": make_token(), "rank": "family"},
        },
    }


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_taxon_list(
    auth_request_factory, taxon_list_view, make_org_with_role, user, role
):
    """List the taxa on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    url = reverse("sepal_v1:taxon-list", kwargs={"org_pk": org.pk})
    req = auth_request_factory("get", url)
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 200
    assert res.data["results"] == [TaxonSerializer(taxon).data]


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_taxon_list_search(
    auth_request_factory, taxon_list_view, make_org_with_role, user, role
):
    """List the taxa on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)  # another taxon in same org
    TaxonFactory(organization=org)  # another taxon in same org
    TaxonFactory(
        name=taxon.name, organization=OrganizationFactory()
    )  # another taxon in different org with same name

    url = reverse("sepal_v1:taxon-list", kwargs={"org_pk": org.pk})
    url += f"?search={taxon.name}"
    req = auth_request_factory("get", url)
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 200
    assert res.data["count"] == 1
    assert len(res.data["results"]) == 1
    assert res.data["results"][0]["id"] == taxon.id


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_taxon_list_search_rank(
    auth_request_factory, taxon_list_view, make_org_with_role, user, role
):
    """List the taxa on for an organization succeeds for all roles."""
    org = make_org_with_role(role)
    # two taxon in same org with same name but different ranks
    genus = TaxonFactory(organization=org, rank="genus")
    TaxonFactory(organization=org, name=genus.name, rank="species")

    url = reverse("sepal_v1:taxon-list", kwargs={"org_pk": org.pk})
    url += f"?search={genus.name}&rank=genus"
    req = auth_request_factory("get", url)
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 200
    assert res.data["count"] == 1
    assert len(res.data["results"]) == 1
    assert res.data["results"][0]["id"] == genus.id


@pytest.mark.django_db
def test_taxon_list_non_org_fails(auth_request_factory, taxon_list_view, non_org_taxon):
    """List the taxa for an organization that the user is not a member of fails."""
    url = reverse(
        "sepal_v1:taxon-list", kwargs={"org_pk": non_org_taxon.organization.pk}
    )
    req = auth_request_factory("get", url)
    res = taxon_list_view(req, org_pk=non_org_taxon.organization.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", role_values)
def test_taxon_detail(
    auth_request_factory, taxon_detail_view, make_org_with_role, role
):
    """Get the details of a taxon succeeds for all roles."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    url = reverse("sepal_v1:taxon-detail", args=[org.pk, taxon.pk])
    req = auth_request_factory("get", url)
    res = taxon_detail_view(req, org_pk=org.pk, pk=taxon.pk)
    assert res.status_code == 200
    assert res.data == TaxonSerializer(taxon).data


@pytest.mark.django_db
def test_taxon_detail_non_org_fails(
    auth_request_factory, taxon_detail_view, organization, non_org_taxon
):
    """Get the the taxon details for an organization that the user is not a member
of fails."""
    url = reverse("sepal_v1:taxon-detail", args=[organization.pk, non_org_taxon.pk])
    req = auth_request_factory("get", url)
    res = taxon_detail_view(req, org_pk=organization.pk, pk=non_org_taxon.pk)
    assert res.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_taxon_create(
    auth_request_factory, taxon_list_view, make_token, make_org_with_role, role
):
    """Create taxon for an organization with owner, admin and write succeeds.."""
    org = make_org_with_role(role)
    url = reverse("sepal_v1:taxon-list", args=[org.pk])
    data = {"name": make_token(), "rank": "family"}
    req = auth_request_factory("post", url, data, format="json")
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 201
    assert res.data == dict(
        **data, default_vernacular_name_id=None, parent_id=None, id=res.data["id"]
    )


# @pytest.mark.django_db
# @pytest.mark.parametrize("role", ["owner", "admin", "write"])
# def test_taxon_create_nested(
#     auth_request_factory,
#     # taxon_bulk_create_view,
#     taxon_list_view,
#     make_org_with_role,
#     role,
#     taxon_nested_data,
# ):
#     org = make_org_with_role(role)
#     url = reverse("sepal_v1:taxon-list", args=[org.pk])
#     req = auth_request_factory("post", url, taxon_nested_data, format="json")
#     res = taxon_list_view(req, org_pk=org.pk)
#     assert res.status_code == 201
#     assert res.data["name"] == taxon_nested_data["name"]
#     assert res.data["rank"] == taxon_nested_data["rank"]


# @pytest.mark.django_db
# @pytest.mark.parametrize("role", ["owner", "admin", "write"])
# def test_taxon_create_nested_existing_parent(
#     auth_request_factory,
#     # taxon_bulk_create_view,
#     taxon_list_view,
#     make_org_with_role,
#     role,
#     taxon_nested_data,
# ):
#     org = make_org_with_role(role)
#     url = reverse("sepal_v1:taxon-list", args=[org.pk])
#     genus = TaxonFactory(rank="genus", organization=org)
#     taxon_nested_data["parent"]["id"] = genus.pk
#     req = auth_request_factory("post", url, taxon_nested_data, format="json")
#     res = taxon_list_view(req, org_pk=org.pk)
#     assert res.status_code == 201
#     assert res.data["name"] == taxon_nested_data["name"]
#     assert res.data["rank"] == taxon_nested_data["rank"]

#     taxon = Taxon.objects.get(pk=res.data["id"])
#     taxon.parent_id = genus.id


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_taxon_create_permission_fails(
    auth_request_factory, taxon_list_view, make_token, make_org_with_role, role
):
    org = make_org_with_role(role)
    url = reverse("sepal_v1:taxon-list", args=[org.pk])
    data = {"name": make_token(), "rank": "family"}
    req = auth_request_factory("post", url, data, format="json")
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_taxon_create_non_org_fails(auth_request_factory, taxon_list_view, make_token):
    org = OrganizationFactory()
    url = reverse("sepal_v1:taxon-list", args=[org.pk])
    data = {"name": make_token(), "rank": "family"}
    req = auth_request_factory("post", url, data, format="json")
    res = taxon_list_view(req, org_pk=org.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_taxon_update(
    auth_request_factory, taxon_detail_view, make_token, make_org_with_role, role
):
    """Update taxon for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org, name=make_token(), rank="species")
    url = reverse("sepal_v1:taxon-detail", args=[org.pk, taxon.pk])
    data = {"name": make_token(), "rank": "family"}
    req = auth_request_factory("put", url, data, format="json")
    res = taxon_detail_view(req, org_pk=org.pk, pk=taxon.pk)
    assert res.status_code == 200
    assert res.data == dict(
        **data, default_vernacular_name_id=None, parent_id=None, id=res.data["id"]
    )


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["owner", "admin", "write"])
def test_taxon_partial_update(
    auth_request_factory, taxon_detail_view, make_token, make_org_with_role, role, taxon
):
    """Update taxon for an organization with owner, admin and write succeeds."""
    org = make_org_with_role(role)
    parent = TaxonFactory(organization=org, name=make_token(), rank="genus")
    new_parent = TaxonFactory(organization=org, name=make_token(), rank="genus")
    taxon = TaxonFactory(
        organization=org, name=make_token(), rank="species", parent=parent
    )
    url = reverse("sepal_v1:taxon-detail", args=[org.pk, taxon.pk])
    data = {"name": make_token(), "parent_id": new_parent.id}

    req = auth_request_factory("patch", url, data, format="json")
    res = taxon_detail_view(req, org_pk=org.pk, pk=taxon.pk)
    assert res.status_code == 200
    assert res.data == dict(
        **data, rank="species", default_vernacular_name_id=None, id=res.data["id"]
    )


@pytest.mark.django_db
def test_taxon_delete(auth_request_factory, taxon_detail_view, organization, taxon):
    url = reverse("sepal_v1:taxon-detail", args=[organization.pk, taxon.pk])
    req = auth_request_factory("delete", url)
    res = taxon_detail_view(req, org_pk=organization.pk, pk=taxon.pk)
    assert res.status_code == 204


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_taxon_delete_permissions_fails(
    auth_request_factory, taxon_detail_view, make_org_with_role, role
):
    """Test the a user can't delete a taxon they don't own."""
    org = make_org_with_role(role)
    taxon = TaxonFactory(organization=org)
    url = reverse("sepal_v1:taxon-detail", args=[org.pk, taxon.pk])
    req = auth_request_factory("delete", url)
    res = taxon_detail_view(req, org_pk=org.pk, pk=taxon.pk)
    assert res.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize("role", ["read"])
def test_taxon_meta(auth_request_factory, make_org_with_role, role):
    org = make_org_with_role(role)
    url = reverse("sepal_v1:taxon-meta", args=[org.pk])
    req = auth_request_factory("get", url)
    view = TaxonViewSet.as_view({"get": "meta"})
    res = view(req, org_pk=org.pk)
    ranks = res.data["ranks"]
    assert len(ranks) > 0
