import pytest

from .fixtures import *  # noqa: F401, F403
from .factories import OrganizationFactory
from sepal.v1.users.serializers import UserSerializer


@pytest.mark.django_db
def test_user_serialize(organization, user):
    data = UserSerializer(user).data
    assert data["id"] == user.id
    assert data["username"] == user.username
    assert data["email"] == user.email
    assert "password" not in data


@pytest.mark.django_db
def test_user_create(make_token, taxon):
    password = make_token()
    data = {"username": make_token(), "password": password}
    serializer = UserSerializer(data=data)
    assert serializer.is_valid(), serializer.errors
    user = serializer.save()
    assert user.id is not None
    assert user.username == data["username"]
    assert user.check_password(password)


@pytest.mark.django_db
def test_user_create_default_invalid_org_fail(organization, make_token, taxon):
    data = {"username": make_token(), "default_organization_id": organization.id}
    serializer = UserSerializer(data=data)
    assert not serializer.is_valid()
    assert "default_organization_id" in serializer.errors


@pytest.mark.django_db
def test_user_partial_update(organization, make_token, user, taxon):
    new_org = OrganizationFactory()
    user.organizations.add(new_org)
    data = {"username": make_token(), "default_organization_id": new_org.id}
    serializer = UserSerializer(user, data=data, partial=True)
    assert serializer.is_valid(), serializer.errors
    user = serializer.save()
    assert user.username == data["username"]
    assert user.profile.default_organization == new_org
