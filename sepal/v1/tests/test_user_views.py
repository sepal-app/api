import pytest
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from sepal.v1.models import User
from sepal.v1.users.serializers import UserSerializer
from sepal.v1.users.views import UserViewSet
from .factories import UserFactory
from .fixtures import *  # noqa: F401, F403


@pytest.fixture
def user_list_view():
    return UserViewSet.as_view({"get": "list", "post": "create"})


@pytest.fixture
def user_detail_view():
    return UserViewSet.as_view(
        {
            "get": "retrieve",
            "delete": "destroy",
            "put": "update",
            "patch": "partial_update",
        }
    )


@pytest.fixture
def user_password_view():
    return UserViewSet.as_view({"post": "password"})


@pytest.mark.django_db
def test_user_detail(auth_request_factory, user_detail_view, user):
    url = reverse("sepal_v1:user-detail", args=[user.pk])
    req = auth_request_factory("get", url)
    res = user_detail_view(req, pk=user.pk)
    assert res.status_code == 200
    print(res.data)
    assert res.data == UserSerializer(user).data


@pytest.mark.django_db
def test_user_detail_other(auth_request_factory, user_detail_view, user):
    user2 = UserFactory()
    url = reverse("sepal_v1:user-detail", args=[user2.pk])
    req = auth_request_factory("get", url)
    res = user_detail_view(req, pk=user2.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_user_list_403(auth_request_factory, user_list_view, user):
    url = reverse("sepal_v1:user-list")
    req = auth_request_factory("get", url)
    res = user_list_view(req, pk=user.pk)
    assert res.status_code == 403


@pytest.mark.django_db
def test_user_create(auth_request_factory, user_list_view, make_token, faker):
    url = reverse("sepal_v1:user-list")
    data = {
        "username": make_token(),
        "email": faker.email(),
        "first_name": faker.first_name(),
        "last_name": faker.last_name(),
        "password": make_token(),
    }
    req = APIRequestFactory().post(url, data, format="json")
    res = user_list_view(req)
    assert res.status_code == 201
    user = User.objects.get(pk=res.data["id"])
    user.check_password(data.pop("password"))
    assert res.data == dict(
        **data, id=res.data["id"], default_organization_id=None, organizations=[]
    )


@pytest.mark.django_db
def test_user_change_password(
    auth_request_factory, user_password_view, user, make_token
):
    password = make_token()
    new_password = make_token()
    user.set_password(password)
    url = reverse("sepal_v1:user-password", args=[user.pk])
    data = {"current_password": password, "password": new_password}
    req = auth_request_factory("post", url, data, format="json")
    res = user_password_view(req, pk=user.pk)
    assert res.status_code == 200
    user.refresh_from_db()
    assert user.check_password(new_password)


@pytest.mark.django_db
def test_user_change_password_403(
    auth_request_factory, user_password_view, user, make_token
):
    password = make_token()
    new_password = make_token()
    user2 = UserFactory()
    user.set_password(password)
    url = reverse("sepal_v1:user-password", args=[user2.pk])
    data = {"current_password": password, "password": new_password}
    req = auth_request_factory("post", url, data, format="json")
    res = user_password_view(req, pk=user2.pk)
    assert res.status_code == 403
    user.refresh_from_db()
    assert not user2.check_password(password)
