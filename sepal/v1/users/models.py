from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from ..models.base import TimestampedModelMixin


class Profile(TimestampedModelMixin):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    default_organization = models.ForeignKey(
        "Organization", on_delete=models.SET_NULL, null=True
    )

    @property
    def organizations(self):
        return self.user.organizations


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    else:
        instance.profile.save()
