from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from ..models import Organization, OrganizationUser, User
from ..organizations.serializers import OrganizationSerializer


class ChangePasswordSerializer(serializers.Serializer):
    current_password = serializers.CharField()
    password = serializers.CharField()

    # TODO: need a validator to check the user's current password


class UserSerializer(FlexFieldsModelSerializer):
    organizations = OrganizationSerializer(
        many=True, read_only=True, source="profile.organizations"
    )

    expandable_fields = {
        "organizations": (OrganizationSerializer, {"many": True, "read_only": True})
    }

    def validate_password(self, value):
        if value is None and not self.instance:
            raise serializers.ValidationError(
                {"password": "A password is required for new users."}
            )
        return value

    default_organization_id = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(),
        source="profile.default_organization",
        required=False,
        allow_null=True,
    )

    def validate_default_organization_id(self, value):
        user_in_org = OrganizationUser.objects.filter(
            user=self.instance, organization=value
        ).exists()
        if not user_in_org:
            raise serializers.ValidationError(
                {
                    "default_organization_id": "The user is not a member of this organization."  # noqa: E501
                }
            )
        return value

    def create(self, validated_data):
        profile_data = validated_data.pop("profile", {})
        password = validated_data.pop("password")
        user = User.objects.create(**validated_data)
        for key, value in profile_data.items():
            setattr(user.profile, key, value)
        user.set_password(password)
        user.save()
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile", {})
        super().update(instance, validated_data)
        for key, value in profile_data.items():
            setattr(instance.profile, key, value)
        instance.profile.save()
        return instance

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "username",
            "email",
            "organizations",
            "default_organization_id",
            "password",
        )
        extra_kwargs = {"password": {"write_only": True}}
