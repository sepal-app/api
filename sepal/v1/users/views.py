from rest_flex_fields import FlexFieldsModelViewSet, is_expanded
from rest_framework.decorators import action, api_view
from rest_framework.permissions import BasePermission, IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from .serializers import UserSerializer, ChangePasswordSerializer
from .models import User

# from ..permissions import HasOrgRoleAction

# TODO: should only be able to create, get and delete your own user on /users

# TODO: should be able to list, get, create and delete and users on /orgs/users
# if your an admin of an organization


class IsCurrentUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        print(f"IS USER: {obj == request.user}")
        return obj == request.user or getattr(obj, "user", None) == request.user


@api_view()
def me(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


class UserViewSet(FlexFieldsModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permit_list_expands = ["organizations"]

    def get_queryset(self):
        queryset = User.objects.all()
        if is_expanded(self.request, "organizations"):
            queryset = queryset.prefetch_related("organizations")
        return queryset

    def get_permissions(self):
        if self.action == "list":
            # Only admins can list users
            permission_classes = [IsAdminUser, IsAuthenticated]
        elif self.action == "create":
            # Anyone can create a user
            permission_classes = []
        else:
            # Other actions can only be done by the requesting user
            permission_classes = [IsAuthenticated, IsCurrentUser]
        return [permission() for permission in permission_classes]

    @action(detail=True, methods=["post"])
    def password(self, request, pk=None):
        user = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            user.set_password(serializer.data["password"])
            user.save()
            return Response(status=200)
        else:
            return Response(serializer.errors, status=400)
