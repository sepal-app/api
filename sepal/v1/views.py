import traceback

from django.http import JsonResponse
from rest_framework.views import exception_handler
from rest_framework.exceptions import ValidationError

from sepal.log import log


def unhandled_exception_handler(exception, context):
    # call the default exception handler
    response = exception_handler(exception, context)
    if isinstance(exception, ValidationError):
        # return validation errors
        return JsonResponse(
            {"detail": getattr(response, "data", "")},
            status=getattr(response, "status_code", 500),
        )

    detail = "Unknown error"
    if response is not None and response.data is not None:
        detail = response.data.get("detail", detail)
    log.error(traceback.format_exc())
    status = response.status_code if response is not None else 500
    return JsonResponse({"detail": detail}, status=status)
