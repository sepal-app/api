#!/bin/bash
. venv/bin/activate
python3 manage.py migrate && gunicorn -c gunicorn.cfg sepal.wsgi
